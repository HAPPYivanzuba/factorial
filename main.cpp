#include <iostream>
using namespace std;


unsigned long long factorial_recursive(const unsigned int num)
{
    if (num<=1) return 1;
    return num*factorial_recursive(num-1);
}

unsigned long long factorial(const unsigned int num)
{

    unsigned long long res=1;
    unsigned long long prev=num;
    while(prev!=1)
    {

        prev--;
        res*=prev;
    }
    return res;
}


int main()
{
    unsigned int max_object=1;

    while(factorial_recursive(max_object)!=0)
    {
        max_object++;
    }
    max_object--;



    unsigned int a=0;
    setlocale(LC_ALL ,"Rus");
    cout<<"Введите число, факториал которого над найти (не более " << max_object << "): "<<endl;
    cin>>a;
    while (a>max_object)
    {
        cout<<"Слишком большое число, невозможно вычислить факториал, выберите другое: "<<endl;
        cin>>a;
    }
    cout << factorial_recursive(a) << endl;
    return 0;
}
/*
factorial=prchislo;//5
do
{
prchislo=prchislo-1;//4
factorial=prchislo*factorial;//20
}while(prchislo!=1);//4
cout<<factorial<<endl;//20
*/
